package leetcode

// Given a roman numeral, convert it to an integer
func romanToInt(s string) int {
	r := 0
	for i := 0; i < len(s); i++ {
		s1 := value(s[i])
		if i+1 < len(s) {
			s2 := value(s[i+1])

			if s1 >= s2 {
				r = r + s1
			} else {
				r = r + s2 - s1
				i++
			}
		} else {
			r = r + s1
		}
	}
	return r
}

func value(c byte) int {
	if c == 'I' {
		return 1
	} else if c == 'V' {
		return 5
	} else if c == 'X' {
		return 10
	} else if c == 'L' {
		return 50
	} else if c == 'C' {
		return 100
	} else if c == 'D' {
		return 500
	} else if c == 'M' {
		return 1000
	} else {
		return 0
	}

}
