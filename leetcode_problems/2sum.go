package leetcode

//this is the first problem of adding numbers of an array to get the given target
// Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

// You may assume that each input would have exactly one solution, and you may not use the same element twice.

// You can return the answer in any order.

// Example 1:
// Input: nums = [2,7,11,15], target = 9
// Output: [0,1]
// Output: Because nums[0] + nums[1] == 9, we return [0, 1].

func twoSum(nums []int, target int) []int {
	var Output []int
	for i, num := range nums {
		for j, seNum := range nums {
			if i != j {
				if num+seNum == target {
					Output = []int{j, i}
					break
				} else {
					continue
				}
			} else {
				continue
			}
		}
	}
	return Output
}
