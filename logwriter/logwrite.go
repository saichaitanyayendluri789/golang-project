package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"time"
)

func main() {

	t := time.Now()
	fmt.Println(t)

	f, err := os.OpenFile("app.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	log.SetOutput(f)
	log.Println("application started")

	result, err := div(1.0, 2.0)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%.2f", result)

}
func div(a, b float64) (ret float64, err error) {
	if b == 0 {
		return 0.0, errors.New("can't divide by 0")
	}
	return a / b, nil
}
