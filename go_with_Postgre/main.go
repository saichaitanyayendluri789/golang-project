package main

import (
	"fmt"
	"log"
	"os"
)

func main() {

	f, err := os.OpenFile("app.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	log.SetOutput(f)

	xconnect()

	fmt.Println("helloworld")
}
