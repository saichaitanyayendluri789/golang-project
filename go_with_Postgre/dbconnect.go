package main

import (
	"log"
	"os"

	pg "github.com/go-pg/pg"
)

//Connect is exported to main for connection
func xconnect() {

	opts := &pg.Options{
		User:     "postgres",
		Password: "root",
		Addr:     "localhost:5432",
	}
	var db *pg.DB = pg.Connect(opts)
	if db == nil {
		log.Println("failed to connect to database")
		os.Exit(100)
	}
	log.Println(" connection successful to the database")
	err := createProductTable(db)
	if err != nil {
		log.Printf("error crerating a table : %v\n", err)
	}
	closeerr := db.Close()
	if closeerr != nil {
		log.Printf("error closing the connection: %v\n", closeerr)

	}
	log.Printf("connection closed successfully.\n ")
	return

}
