package main

import (
	"log"
	"time"

	pg "github.com/go-pg/pg"
	orm "github.com/go-pg/pg/orm"
)

// Product is ...
type Product struct {
	tableName struct{}  `sql:"product_item_collection"`
	ID        int       `sql:"id,pk"`
	Name      string    `sql:"name,unique"`
	Descript  string    `sql:"description"`
	Image     string    `sql:"image"`
	Price     float64   `sql:"price,type:real"`
	Features  string    `sql:"features"`
	CreatedAt time.Time `sql:"created_at"`
	UpdatedAt time.Time `sql:"updated_at"`
	IsActive  bool      `sql:"is_active"`
}

func createProductTable(db *pg.DB) error {
	opts := &orm.CreateTableOptions{
		IfNotExists: true,
	}
	createErr := orm.CreateTable(db, &Product{}, opts)

	if createErr != nil {
		log.Printf("Error creating Database: %v", createErr)
	}
	log.Println("created successfully")
	return nil
}
