Operating system:

	The operating system is a set of instructions that abstacts the physical aspects of a CPU such as Memory, Cores of the processor,
Cache systems, registerfiles and computing units in such a way that the software application running can access them directly without the 
knowledge of the physical model.

	it has 2 spaces namely Userspace and Kernel space. Userspace gives full access to the user but kernel is restricted to the admin or 
developer only. any incorrect changes in the kernel files may currupt the whole operating system and my crash it.

CPU , Threads: 
	
	Cpu is the processing unit of the computer which consists of Processing unit and Memory unit in the processing unit it consists of
 detailed explanation of the cores of the processor , cache systems, logic of execution and micro architecture of the system. 

	Threads are the virtual cores that executes the given program and helps in parellel procesing of multiple programs at the same time
each thread. threads follow order of execution and are processed in such a way to avoid as many conflicts as possible with other threads at 
any given time. A system at any given time can process double the threads to its number of cores. arm architecture uses Simultaneous Multithreading
 for running 2 threads on a core and intel's X86 and similar architectures use hyper threading to achive such optimization


Stack Memory, Heap Memory: 
	
	Stack memory is a limited space of memory used by the processor where the compiler allocates memory for given variables during compile time.
these are fixed memories and these alloted memory spaces will be dealloced after the end of the program execution. a developer or programmer need not 
worry about the memory issues when using the stack memory

	Heap memory is the larger part of the memory where the allotment of memory is done during the run time and user has to deallocate the momory 
after done with the process. usually it is used for dynamic allocation while using linked lists or array lists where the size is unknown upfront and
has to be allocated dynamically. If the user or the programmer doesnot deallocate then the allocated spaces will not be cleared and will be filled up
indefinitely. which may cause memory issues with un-usable memory spaces.

Kernel Signals:

	Kernel signals are the system calls used by the kernel to control the flow of execution of a process on the cpu. The kernel issues the system calls
to the processor to determine what action to be taken. The kernel determines the action and sends signal using the process ID and signal number.

The most common signals are as follows: KILL, STOP, READ, WRITE, EXEC , QUIT, MASK, send interrupt , handle the interrupt, suspend the process, 
switch from one process to other, exit .
 
GOMAXPROCS:
	It is a go runtime command used to allot the n number of CPUs to execute simultaneously where n is always less than the total number of logical CPUs 
on the system.
	the syntax is GOMAXPROCS(n int) int 
		this instruction takes number of logical CPUs to be alloted and returns the number of Logical Cpus previously alloted 

OS signals:
	OS signals can be defines as the communication signals between two go routiens that are running paralelly through a channel

Go Runtime Scheduling:
	go runtime scheduling manages the scheduling of go routiens, garbage collection, also manages the runtime environment.
	the runtime can be defines as the set of operations to interact with go's functions of control go routiens
	the go runtime uses M:N scheduler where m is the number of go routiens and N is the number of OS Threads
	we need to create go routiens such that they run concurrently , unlike the single threaded way
	Go routines are Light weighted threads with dynamic stack size 