package main

import "fmt"

func main() {

	cards := newdeck()

	hand, remainingDeck := deal(cards, 2)

	fmt.Println("Hand is\n ")

	hand.print()
	fmt.Println("\nremaing Deck after Dealing is    ")
	remainingDeck.print()

}
