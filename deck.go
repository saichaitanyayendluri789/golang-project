package main

import "fmt"

type deck []string

func newdeck() deck {
	cards := deck{}

	cardSuits := []string{"Spades", "Hearts", "Daimonds", "Clubs"}
	cardValues := []string{"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"}

	for _, Suite := range cardSuits {
		for _, Value := range cardValues {
			cards = append(cards, Value+" of "+Suite)
		}
	}
	return cards
}

func (d deck) print() {
	for _, card := range d {
		fmt.Println(card)
	}
}

func deal(d deck, handSize int) (deck, deck) {
	return d[:handSize], d[handSize:]

}
